def parse_plan(domain,problem,plan):
    def tokenize(line):
        # tokens = line.replace("Deleting",'').replace("Adding",'').replace("(",'').replace(")",'').strip().split()
        token = line.replace("Deleting",'').replace("Adding",'').strip()
        return token
        
    import os,subprocess
    VAL = os.path.join(os.path.dirname(os.path.realpath(__file__)),"ext/VAL/validate")
    val_output = subprocess.check_output("%s -v %s %s %s" % (VAL,domain,problem,plan), shell=True).decode('UTF-8')
    steps = val_output.split("Checking next happening")[1:]
    
    deltas = [] # each delta is a pair (adds,dels)
    # if len(steps) == 1:
    #     # plan has length 0
    #     return deltas
    # else:
    # steps = steps[1:]
    for step in steps:
        adds = []
        dels = []
        for line in step.split('\n'):
            if line.startswith("Deleting "):
                dels.append(tokenize(line))
            if line.startswith("Adding"):
                adds.append(tokenize(line))
        deltas.append( (adds,dels) )
    return deltas


def parse_initial_state(domain,problem):
    import os,sys
    sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)),"ext/pddl_parser"))
    from PDDL import PDDL_Parser

    pddl_parser = PDDL_Parser()
    pddl_parser.parse_domain(domain)
    pddl_parser.parse_problem(problem)
    tokenized_initial_state = pddl_parser.state # each predicate is a list of string tokens
    
    initial_state = []
    for predicate_tokens in tokenized_initial_state:
        initial_state.append( "(%s)" % ' '.join(predicate_tokens))
    
    return initial_state # each predicate is a parenthesized string


def get_state_plan(domain,problem,plan):
    
    # each predicate is a parenthesized string
    initial_state = parse_initial_state(domain,problem)
    
    # assign an ID to each predicate
    predicates_dict = {}
    for predicate in initial_state:
        if predicate not in predicates_dict:
            predicates_dict[predicate] = len(predicates_dict)
    
    # each predicate is a predicate ID number
    log_initial_state = set(predicates_dict[predicate] for predicate in initial_state)

    log_states_plan = []
    log_states_plan.append(log_initial_state)
    
    state_plan_deltas = parse_plan(domain,problem,plan)
    for (adds,dels) in state_plan_deltas:

        for predicate in adds + dels:
            if predicate not in predicates_dict:
                predicates_dict[predicate] = len(predicates_dict)
        
        log_adds = set(predicates_dict[predicate] for predicate in adds)
        log_dels = set(predicates_dict[predicate] for predicate in dels)
        
        # copy the previous logical state, plus adds, minus deletes
        cur_log_state = log_states_plan[-1].union(log_adds).difference(log_dels)
        # append the logical state to the plan
        log_states_plan.append(cur_log_state)

    # create a list to reverse the predicates_dict dictionary
    vars_list = ['']* len(predicates_dict)
    for key in predicates_dict:
        vars_list[predicates_dict[key]] = key
    
    # return the states plan
    states_plan = []
    for log_state in log_states_plan:
        state = [ vars_list[var] for var in log_state ]
        states_plan.append(state)
    
    return(states_plan)
        

# ==========================================
# Main
# ==========================================
if __name__ == '__main__':
    import sys
    import pprint
    domain = sys.argv[1]
    problem = sys.argv[2]
    plan = sys.argv[3]
    
    # each predicate is a parenthesized string
    initial_state = parse_initial_state(domain,problem)
    
    
    # assign an ID to each predicate
    predicates_dict = {}
    for predicate in initial_state:
        if predicate not in predicates_dict:
            predicates_dict[predicate] = len(predicates_dict)

    
    log_initial_state = set(predicates_dict[predicate] for predicate in initial_state)
    print(initial_state)
    print(log_initial_state)

    log_states_plan = []
    log_states_plan.append(log_initial_state)
    
    state_plan_deltas = parse_plan(domain,problem,plan)
    for (adds,dels) in state_plan_deltas:

        for predicate in adds + dels:
            if predicate not in predicates_dict:
                predicates_dict[predicate] = len(predicates_dict)
        
        log_adds = set(predicates_dict[predicate] for predicate in adds)
        log_dels = set(predicates_dict[predicate] for predicate in dels)
        
        # copy the previous logical state, plus adds, minus deletes
        cur_log_state = log_states_plan[-1].union(log_adds).difference(log_dels)
        print(cur_log_state)
        
        log_states_plan.append(cur_log_state)


    vars_list = ['']* len(predicates_dict)
    for key in predicates_dict:
        vars_list[predicates_dict[key]] = key
    
    states_plan = []
    for log_state in log_states_plan:
        state = [ vars_list[var] for var in log_state ]
        states_plan.append(state)
    
        print(state)
        