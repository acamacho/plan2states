PLAN2STATES
===========

This tool transforms an action plan into a states plan.

Usage
-----
    
    python plan2states.py <domain_file> <instance_file> <plan_file>

Example:

    python plan2states.py example/p01-domain.pddl example/p01-airport1-p1.pddl example/sas_plan


Installation
------------
The following command to download all dependencies.

    git submodule update --init



PLAN2STATES depends on the following tools:

[VAL](https://github.com/KCL-Planning/VAL.git): A PDDL plan validator.


[PDDL Parser](https://github.com/pucrs-automated-planning/pddl-parser.git): A simple parser.

    